const express = require('express');

const server = express();

server.use(express.json());

projects = [];

totalReq = 0

server.use((req, res, next) => {
    console.time('request');

    console.log(++totalReq);

    next();

    console.timeEnd('request');
});

function checkProjectData(req, res, next) {

    if (!req.body.id) {
        return res.status(400).json({error:'Project ID is required'});
    }

    if (!req.body.title) {
        return res.status(400).json({error:'Project TITLE is required'});
    }

    return next();
};

function checkProjectTitle(req, res, next) {

    if (!req.body.title) {
        return res.status(400).json({error:'Project TITLE is required'});
    }

    return next();
};

function checkProjectInArray(req, res, next) {

    const {id} = req.params;

    console.log("id", id);
  
    // Here all the elements of the array is being printed. 
    function getProjectIndex(){
        for (let i = 0; i< projects.length; i++) {
            if (projects[i].id === id){
                return i;
            }
        } 
    }

    const projIndex = getProjectIndex();

    const project = projects[projIndex];

    console.log("project",project);

    if (!project) {
        return res.status(400).json({error : 'Project does not exists'});
    }

    req.project = project;
    req.projectIndex = projIndex;
    
    return next();
};

server.post('/projects', checkProjectData, (req, res) => {

    const { id, title } = req.body;

    projects.push({id, title, tasks:[]});

    return res.json(projects[projects.length -1]);
});

server.post('/projects/:id/tasks', checkProjectTitle, checkProjectInArray, (req, res) => {

    console.log("projects[req.projectIndex]", projects[req.projectIndex])
    console.log("req.body.title", req.body.title)

    console.log("projects[req.projectIndex].tasks", projects[req.projectIndex].tasks)
    
    projects[req.projectIndex].tasks.push(req.body.title);

    console.log("projects[req.projectIndex] after", projects[req.projectIndex])

    return res.json(projects[req.projectIndex]);
});

server.get('/projects', (req, res) => {

    return res.json(projects);

});

server.put('/projects/:id', checkProjectInArray, (req, res) => {

    req.project.title = req.body.title;

    projects[req.projectIndex] = req.project;

    return res.json(projects[req.projectIndex]);

});

server.delete('/projects/:id', checkProjectInArray, (req, res) => {

    projects.splice(req.projectIndex, 1);

    return res.send("sucesso!")

});

server.listen(3001, function(){
    console.log("info",'Server is running at port : ' + 3000);
});